require 'spec_helper'

describe Mks::Common do
  it 'has a version number' do
    expect(Mks::Common::VERSION).not_to be nil
  end

  it 'allows creating a new methodresponse' do
    response = Mks::Common::MethodResponse.new(true, "test message", nil, 0)
    expect(response.success).to eq(true)
  end

  # it 'serializes object to json' do
  #   obj = MethodResponse.new(true, "test message", "test data", 0)
  #   json = obj.to_json
  #   expect(json).to eq '{"success":true,"message":"test message","data":"test data","total":0}'
  # end

  it 'converts object to hash' do
    obj = Mks::Common::MethodResponse.new(true, "test message", "test data", 0)
    expect(obj.to_hash.has_key? "success").to be_truthy
    expect(obj.to_hash.has_key? "message").to be_truthy
    expect(obj.to_hash.has_key? "data").to be_truthy
    expect(obj.to_hash.has_key? "total").to be_truthy

  end

  it 'creates object from json string' do
    data = {"success" => true, "message" => "test message", "data" => "test data", "total" => 0,
            "errors" => ["Error 1", "Error 2"]}
    json = JSON.generate(data)
    response = Mks::Common::MethodResponse.from_json json
    expect(response.success).to be_truthy
    expect(response.data).to eq "test data"
    expect(response.message).to eq "test message"
    expect(response.errors.count).to eq 2
    expect(response.total).to eq 0
  end

  
end
